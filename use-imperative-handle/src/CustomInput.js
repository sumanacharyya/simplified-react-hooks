import React, { forwardRef, useImperativeHandle } from "react";

const CustomInput = ({ value, ...args }, ref) => {
  useImperativeHandle(
    ref,
    () => {
      return { alertHi: () => alert("Hi") };
    },
    []
  );

  return <input style={{ backgroundColor: value }} ref={ref} {...args} />;
};

export default forwardRef(CustomInput);
