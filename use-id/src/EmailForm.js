import React, { useId } from "react";

const EmailForm = () => {
  const id = useId();
  return (
    <>
      <label htmlFor={`${id}-name`}>Name </label>
      <input type="name" id={`${id}-name`} />
      <br />
      <label htmlFor={`${id}-email`}>Email </label>
      <input type="email" id={`${id}-email`} />
    </>
  );
};

export default EmailForm;
