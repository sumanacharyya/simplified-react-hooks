import { useState } from "react";
import {useLocalStorage} from "./useLocalStorage";

function App() {
  const [firstName, setFirstName] = useLocalStorage("");
  const [lastName, setLastName] = useState("");

  return (
    <>
      <input
        type="text"
        placeholder="First Name"
        onChange={(e) => setFirstName(e.target.value)}
      />
      <input
        type="text"
        placeholder="Last Name"
        onChange={(e) => setLastName(e.target.value)}
      />
    </>
  );
}

export default App;
