import React, { useEffect, useRef, useState } from "react";

function App() {
  const [name, setName] = useState("");
  const inputRef = useRef();
  const renderCount = useRef(0);
  const prevName = useRef("");

  useEffect(() => {
    renderCount.current = renderCount.current + 1;
    prevName.current = name;
  }, [name]);

  const focus = () => {
    inputRef.current.focus();
  };

  return (
    <>
      <input
        // ref={inputRef}
        type="text"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <div>My name is: {name}</div>
      <div>Name used to be: {prevName.current}</div>
      <button onClick={focus}>Focus Input</button>
      <div>Rendered {renderCount.current} times</div>
    </>
  );
}

export default App;
