import React from "react";
import { ACTIONS } from "./App";

const Todo = ({ todo, dispatch }) => {
  return (
    <div style={{ marginTop: "10px" }}>
      <span style={{ color: todo.done ? "#AAA" : "#111", marginRight: "20px" }}>
        {todo.name}
      </span>
      <button
        style={{ marginRight: "10px" }}
        onClick={() =>
          dispatch({
            type: ACTIONS.DONE_TODO,
            payload: { id: todo.id },
          })
        }
      >
        Mark Done
      </button>
      <button
        onClick={() =>
          dispatch({
            type: ACTIONS.REMOVE_TODO,
            payload: { id: todo.id },
          })
        }
      >
        Delete Todo
      </button>
    </div>
  );
};

export default Todo;
