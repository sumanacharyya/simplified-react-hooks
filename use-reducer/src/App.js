import React, { useReducer, useState } from "react";
import Todo from "./Todo";

export const ACTIONS = {
  // INCREMENT: "increment",
  // DECREMENT: "decrement",
  ADD_TODO: "add-todo",
  REMOVE_TODO: "remove-todo",
  DONE_TODO: "add-DONE",
};

// function reducerFunc(state, action) {
//   switch (action.type) {
//     case ACTIONS.INCREMENT:
//       return { ...state, count: state.count + action.payload };
//     case ACTIONS.DECREMENT:
//       return { ...state, count: state.count - action.payload };

//     default:
//       return state;
//   }
// }

function todoReducer(state, action) {
  switch (action.type) {
    case ACTIONS.ADD_TODO:
      return [...state, action.payload];
    case ACTIONS.REMOVE_TODO:
      return state.filter((todo) => todo.id !== action.payload.id);
    case ACTIONS.DONE_TODO:
      return state.map((todo) => {
        if (todo.id === action.payload.id) {
          return { ...todo, done: !todo.done };
        }
        return todo;
      });

    default:
      break;
  }
}

function App() {
  const [todos, dispatch] = useReducer(todoReducer, []);
  const [name, setName] = useState("");

  // const [state, dispatch] = useReducer(reducerFunc, { count: 0 });
  // // const [count, setCount] = useState(0);

  // function incrementCount() {
  //   // setCount((precCount) => precCount + 1);
  //   dispatch({
  //     type: ACTIONS.INCREMENT,
  //     payload: 1,
  //   });
  // }

  // function decrementCount() {
  //   // setCount((precCount) => precCount - 1);
  //   dispatch({
  //     type: ACTIONS.DECREMENT,
  //     payload: 1,
  //   });
  // }

  function todoSubmitHandler(e) {
    e.preventDefault();
    if (name) {
      dispatch({
        type: ACTIONS.ADD_TODO,
        payload: { id: name, name, done: false },
      });
    }

    setName("");
  }

  return (
    <div>
      {/* <button onClick={decrementCount}>-</button>
      <span>{state.count}</span>
      <button onClick={incrementCount}>+</button> */}

      <form onSubmit={todoSubmitHandler}>
        <input
          type="text"
          onChange={(e) => setName(e.target.value)}
          value={name}
        />
        <button>Add Todo</button>
      </form>
      <div>
        {todos.map((eachObj) => (
          <Todo key={eachObj.id} todo={eachObj} dispatch={dispatch} />
        ))}
      </div>
    </div>
  );
}

export default App;
