import React, { useMemo, useDeferredValue } from "react";

const List = ({ input }) => {
  const LIST_SIZE = 20000;
  const deferredInput = useDeferredValue(input);

  const list = useMemo(() => {
    const lst = [];
    for (let i = 0; i < LIST_SIZE; i++) {
      lst.push(<div key={i}>{deferredInput}</div>);
    }
    return lst;
  }, [deferredInput]);

  return list;
};

export default List;
