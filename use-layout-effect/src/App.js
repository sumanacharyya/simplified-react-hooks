import { useLayoutEffect, useState } from "react";

function App() {
  const [count, setCount] = useState(0);

  useLayoutEffect(() => {
    console.log(count);
  }, [count]);

  return (
    <>
      <button onClick={(e) => setCount((prev) => prev + 1)}>Increment</button>
      <div>{count}</div>
    </>
  );
}

export default App;
