import React, { useLayoutEffect, useRef, useState } from "react";

const ModalExample = () => {
  const [show, setShow] = useState(false);
  const popup = useRef();
  const button = useRef();

  useLayoutEffect(() => {
    if (popup.current == null || button.current == null) return;
    const { bottom } = button.current.getBoundingClientRect();
    popup.current.style.top = `${bottom + 25}px`;
  }, [show]);

  return (
    <>
      <button onClick={() => setShow((prev) => !prev)} ref={button}>
        Click Here
      </button>

      {show && (
        <div style={{ position: "absolute" }} ref={popup}>
          This is PopUp...!
        </div>
      )}
    </>
  );
};

export default ModalExample;
