import { useState, useTransition } from "react";

function App() {
  const [isPending, startTransition] = useTransition();
  const [input, setInput] = useState("");
  const [list, setList] = useState([]);

  const LIST_SIZE = 20000;

  function handleChange(e) {
    e.preventDefault();
    setInput(e.target.value);
    startTransition(() => {
      const lst = [];
      for (let i = 0; i < LIST_SIZE; i++) {
        lst.push(e.target.value);
      }
      setList(lst);
    });
  }

  return (
    <>
      <input type="text" value={input} onChange={handleChange} />
      {isPending ? (
        <p>Loading... </p>
      ) : (
        list.map((item, index) => <div key={index}>{item}</div>)
      )}
    </>
  );
}

export default App;
