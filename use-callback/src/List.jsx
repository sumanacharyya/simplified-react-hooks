import React, { useEffect, useState } from "react";

const List = ({ getItems }) => {
  const [items, setItems] = useState([]);

  useEffect(() => {
    setItems(getItems(3));
    console.log("Updated 'getItems'");
  }, [getItems]);

  return items.map((eachVal) => <div key={eachVal}> {eachVal} </div>);
};

export default List;
