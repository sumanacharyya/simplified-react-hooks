import React from "react";
import FunctionalContextComponent from "./components/FunctionalContextComponent";
// import ClassContextComponent from "./components/ClassContextComponent";
import { ThemeProvider } from "./ThemeContext";

function App() {
  return (
    <ThemeProvider>
      <FunctionalContextComponent />
      {/* <ClassContextComponent /> */}
    </ThemeProvider>
  );
}

export default App;
