import React from "react";
import { useTheme, useThemeUpdate } from "../ThemeContext";

const FunctionalContextComponent = () => {
  const darkTheme = useTheme();
  const themeToggler = useThemeUpdate();

  const theemStyles = {
    backgroundColor: darkTheme ? "#222" : "#DDD",
    color: darkTheme ? "#CCC" : "#333",
    margin: "2rem",
    padding: "3rem",
  };

  return (
    <>
      <button onClick={themeToggler}>Change Theme</button>
      <div style={theemStyles}>Function Based Theme</div>
    </>
  );
};

export default FunctionalContextComponent;
