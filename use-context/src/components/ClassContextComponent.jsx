import React, { Component } from "react";
import { ThemeContext } from "../App";

export default class ClassContextComponent extends Component {
  theemStyles(darkTheme) {
    return {
      backgroundColor: darkTheme ? "#222" : "#DDD",
      color: darkTheme ? "#CCC" : "#333",
      margin: "2rem",
      padding: "3rem",
    };
  }
  render() {
    return (
      <ThemeContext.Consumer>
        {(darkTheme) => (
          <div style={this.theemStyles(darkTheme)}>Class Based Theme</div>
        )}
      </ThemeContext.Consumer>
    );
  }
}
