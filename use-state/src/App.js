import { useState } from "react";
function App() {
  const [count, setCount] = useState(() => ({
    val: 0,
    status: "-",
    appName: "Counter App",
  }));

  const decrement = (e) => {
    e.preventDefault();
    setCount((prev) => ({ ...prev, val: count.val - 1, status: "Down" }));
  };

  const increment = (e) => {
    e.preventDefault();
    setCount((prev) => ({ ...prev, val: count.val + 1, status: "Up" }));
  };

  return (
    <div>
      <h1>{count.appName}</h1>
      <button onClick={decrement}>-</button>
      &nbsp; {count.val}&nbsp;{count.status} &nbsp;
      <button onClick={increment}>+</button>
    </div>
  );
}

export default App;
