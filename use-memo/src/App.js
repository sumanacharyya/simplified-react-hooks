import React, { useState, useMemo } from "react";

function App() {
  const [number, setNumber] = useState(0);
  const [dark, setDark] = useState(false);
  const doubleNumber = useMemo(() => slowFunc(number), [number]);

  const themeStyles = useMemo(() => {
    return {
      backgroundColor: dark ? "#333" : "#FFFFFF",
      color: dark ? "#FFFFFF" : "#222",
    };
  }, [dark]);

  return (
    <div>
      <input
        type="number"
        value={number}
        onChange={(e) => setNumber(parseInt(e.target.value))}
      />
      <button onClick={() => setDark((prev) => !prev)}>Change Theme</button>
      <div style={themeStyles}>{doubleNumber}</div>
    </div>
  );
}

export default App;

function slowFunc(num) {
  console.log("Calling Slow Function!");
  for (let i = 0; i <= 1000000000000000000; i++) {
    return num * 5;
  }
}
