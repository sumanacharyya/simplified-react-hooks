import { useState, useEffect } from "react";

function App() {
  const [resourceType, setResourceType] = useState(() => "posts");
  const [windowWidth, setWindowWidth] = useState(() => window.innerWidth);
  const [windowTrack, setWindowTrack] = useState(() => "-");

  const resizeHandler = () => {
    setWindowWidth(() => window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener("resize", resizeHandler);
    setWindowTrack(() => windowWidth);

    return () => {
      window.removeEventListener("resize", resizeHandler);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [resourceType]);

  return (
    <div>
      <button onClick={() => setResourceType(() => "posts")}>Posts</button>
      <button onClick={() => setResourceType(() => "users")}>Users</button>
      <button onClick={() => setResourceType(() => "comments")}>
        Comments
      </button>
      <br />
      {resourceType}
      <br />
      {windowWidth}
      <br />
      Tracked: {windowTrack}
    </div>
  );
}

export default App;
